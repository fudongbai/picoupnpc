#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include "miniupnpc.h"
#include "upnpreplyparse.h"

#define UPNP_DEFAULT_SERVICE_TYPE "urn:schemas-upnp-org:service:AVTransport:1"

#define ADD_DEVICE_COUNT_STEP 16
static char *
build_absolute_url(const char * baseurl, const char * descURL,
                   const char * url, unsigned int scope_id)
{
	int l, n;
	char * s;
	const char * base;
	char * p;
#if defined(IF_NAMESIZE) && !defined(_WIN32)
	char ifname[IF_NAMESIZE];
#else /* defined(IF_NAMESIZE) && !defined(_WIN32) */
	char scope_str[8];
#endif	/* defined(IF_NAMESIZE) && !defined(_WIN32) */

	if(  (url[0] == 'h')
	   &&(url[1] == 't')
	   &&(url[2] == 't')
	   &&(url[3] == 'p')
	   &&(url[4] == ':')
	   &&(url[5] == '/')
	   &&(url[6] == '/'))
		return strdup(url);
	base = (baseurl[0] == '\0') ? descURL : baseurl;
	n = strlen(base);
	if(n > 7) {
		p = strchr(base + 7, '/');
		if(p)
			n = p - base;
	}
	l = n + strlen(url) + 1;
	if(url[0] != '/')
		l++;
	if(scope_id != 0) {
#if defined(IF_NAMESIZE) && !defined(_WIN32)
		if(if_indextoname(scope_id, ifname)) {
			l += 3 + strlen(ifname);	/* 3 == strlen(%25) */
		}
#else /* defined(IF_NAMESIZE) && !defined(_WIN32) */
		/* under windows, scope is numerical */
		l += 3 + snprintf(scope_str, sizeof(scope_str), "%u", scope_id);
#endif /* defined(IF_NAMESIZE) && !defined(_WIN32) */
	}
	s = malloc(l);
	if(s == NULL) return NULL;
	memcpy(s, base, n);
	if(scope_id != 0) {
		s[n] = '\0';
		if(0 == memcmp(s, "http://[fe80:", 13)) {
			/* this is a linklocal IPv6 address */
			p = strchr(s, ']');
			if(p) {
				/* insert %25<scope> into URL */
#if defined(IF_NAMESIZE) && !defined(_WIN32)
				memmove(p + 3 + strlen(ifname), p, strlen(p) + 1);
				memcpy(p, "%25", 3);
				memcpy(p + 3, ifname, strlen(ifname));
				n += 3 + strlen(ifname);
#else /* defined(IF_NAMESIZE) && !defined(_WIN32) */
				memmove(p + 3 + strlen(scope_str), p, strlen(p) + 1);
				memcpy(p, "%25", 3);
				memcpy(p + 3, scope_str, strlen(scope_str));
				n += 3 + strlen(scope_str);
#endif /* defined(IF_NAMESIZE) && !defined(_WIN32) */
			}
		}
	}
	if(url[0] != '/')
		s[n++] = '/';
	memcpy(s + n, url, l - n);
	return s;
}

/* Prepare the Urls for usage...
 */
void GetUPNPUrls(struct UPNPUrls * urls, struct IGDdatas * data,
            const char * descURL, unsigned int scope_id)
{
	/* strdup descURL */
	urls->rootdescURL = strdup(descURL);

	/* get description of WANIPConnection */
	urls->ipcondescURL = build_absolute_url(data->urlbase, descURL,
	                                        data->first.scpdurl, scope_id);
	urls->controlURL = build_absolute_url(data->urlbase, descURL,
	                                      data->first.controlurl, scope_id);
	urls->controlURL_CIF = build_absolute_url(data->urlbase, descURL,
	                                          data->CIF.controlurl, scope_id);
	urls->controlURL_6FC = build_absolute_url(data->urlbase, descURL,
	                                          data->IPv6FC.controlurl, scope_id);

#ifdef DEBUG
	printf("urlbase='%s'\n", data->urlbase);
	printf("descURL='%s'\n", descURL);
	printf("urls->ipcondescURL='%s'\n", urls->ipcondescURL);
	printf("urls->controlURL='%s'\n", urls->controlURL);
	printf("urls->controlURL_CIF='%s'\n", urls->controlURL_CIF);
	printf("urls->controlURL_6FC='%s'\n", urls->controlURL_6FC);
#endif
}

void FreeUPNPUrls(struct UPNPUrls * urls)
{
	if(!urls)
		return;
	free(urls->controlURL);
	urls->controlURL = 0;
	free(urls->ipcondescURL);
	urls->ipcondescURL = 0;
	free(urls->controlURL_CIF);
	urls->controlURL_CIF = 0;
	free(urls->controlURL_6FC);
	urls->controlURL_6FC = 0;
	free(urls->rootdescURL);
	urls->rootdescURL = 0;
}

void DisplayNameValueList(char * buffer, int bufsize)
{
	struct NameValueParserData pdata;
	struct NameValue * nv;
	ParseNameValue(buffer, bufsize, &pdata);
	for (nv = pdata.l_head;
			nv != NULL; nv = nv->l_next) {
		printf("%s = %s\n", nv->name, nv->value);
	}
	ClearNameValueList(&pdata);
}

//int upnpSetAVTransportURI(const char * controlURL, const char * servicetype)
int upnpSetAVTransportURI(const char * controlURL)
{
	struct UPNParg * SetAVTransportUIRArgs;
	char * buffer;
	int bufsize;

	SetAVTransportUIRArgs = calloc(4, sizeof(struct UPNParg));
	if(SetAVTransportUIRArgs == NULL)
		return -ENOMEM;
	SetAVTransportUIRArgs[0].elt = "InstanceID";
	SetAVTransportUIRArgs[0].val = "0";
	SetAVTransportUIRArgs[1].elt = "CurrentURI";
	SetAVTransportUIRArgs[1].val = "http://192.168.1.1:8200/MediaItems/25.mp4";
	SetAVTransportUIRArgs[2].elt = "CurrentURIMetaData";
	SetAVTransportUIRArgs[2].val = "xxx";
	buffer = simpleUPnPcommand(-1, controlURL, UPNP_DEFAULT_SERVICE_TYPE,
	                           "SetAVTransportURI", SetAVTransportUIRArgs, &bufsize);
	free(SetAVTransportUIRArgs);
	if (!buffer)
		return -1;

	DisplayNameValueList(buffer, bufsize);
	//ParseNameValue(buffer, bufsize, &pdata);
	free(buffer); buffer = NULL;

	return 0;
}

int upnpPlay(const char * controlURL)
{
	struct UPNParg * PlayArgs;
	struct NameValueParserData pdata;
	char * buffer;
	int bufsize;

	PlayArgs = calloc(4, sizeof(struct UPNParg));
	if(PlayArgs == NULL)
		return -ENOMEM;

	PlayArgs[0].elt = "InstanceID";
	PlayArgs[0].val = "0";
	PlayArgs[1].elt = "Speed";
	PlayArgs[1].val = "1";
	buffer = simpleUPnPcommand(-1, controlURL, UPNP_DEFAULT_SERVICE_TYPE,
	                           "Play", PlayArgs, &bufsize);
	free(PlayArgs);
	if (!buffer)
		return -1;

	DisplayNameValueList(buffer, bufsize);
	//ParseNameValue(buffer, bufsize, &pdata);
	free(buffer); buffer = NULL;

	return 0;
}





int main(int argc, char * * argv)
{
	const char * searched_device = NULL;
	const char * multicastif = 0;
	const char * minissdpdpath = 0;
	int ipv6 = 0;
	unsigned char ttl = 2;
	int error = 0;
	struct UPNPDev * devlist = 0;
	struct UPNPDev * dev;
	struct UPNPUrls urls;
	struct IGDdatas data;
	char lanaddr[64] = "unset";	/* my ip address on the LAN */
	int i;
	//const char *url = "http://192.168.1.209:58645/dev/6124b2e8-76c9-792f-0000-00003722e7ff/svc/upnp-org/AVTransport/action";
	const char *url = "http://192.168.1.209:58645/dev/6124b2e8-76c9-792f-ffff-ffffb38d2ced/svc/upnp-org/AVTransport/action";

	searched_device = UPNP_DEFAULT_SERVICE_TYPE;
	printf("searching UPnP device type %s\n", searched_device);
	devlist = upnpDiscoverDevice(searched_device,
		                     2000, multicastif, minissdpdpath,
		                     0/*localport*/, ipv6, ttl, &error);

	if (!devlist) {
		printf("no device found.\n");
		return 1;
	}

	for (dev = devlist, i = 1; dev != NULL; dev = dev->pNext, i++) {
		printf("%3d: %-48s\n", i, dev->st);
		printf("     %s\n", dev->descURL);
		printf("     %s\n", dev->usn);
		//UPNP_GetIGDFromUrl(dev->descURL, &urls, &data, lanaddr, sizeof(lanaddr));



		if ((dev->descURL &&
			UPNP_GetIGDFromUrl(dev->descURL, &urls, &data, lanaddr, sizeof(lanaddr)))) {
			printf("UPnP device found: %s\n", urls.controlURL);
			printf("Local LAN ip address : %s\n", lanaddr);


		}
	}
	putchar('\n');
	freeUPNPDevlist(devlist);

#ifdef DEBUG
	printf("urls->ipcondescURL='%s'\n", urls->ipcondescURL);
	printf("urls->controlURL='%s'\n", urls->controlURL);
	printf("urls->controlURL_CIF='%s'\n", urls->controlURL_CIF);
	printf("urls->controlURL_6FC='%s'\n", urls->controlURL_6FC);
#endif


	upnpSetAVTransportURI(urls.controlURL_CIF);
	upnpPlay(urls.controlURL_CIF);
	FreeUPNPUrls(&urls);

	return 0;
}
